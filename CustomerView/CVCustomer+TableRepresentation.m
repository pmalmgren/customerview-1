//
//  CVCustomer+TableRepresentation.m
//  CustomerView
//
//  Created by Peter Malmgren on 12/19/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import "CVCustomer+TableRepresentation.h"

@implementation CVCustomer (TableRepresentation)

- (NSDictionary *)tr_cellrepresentation {
    NSDateFormatter *df = [NSDateFormatter new];
    if ([[NSLocale availableLocaleIdentifiers] indexOfObject:@"en_US"]
        != NSNotFound) {
        NSLocale* loc = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [df setLocale:loc]; // English month name and time zone name if possible
    }
    [df setDateFormat:@"d MMMM yyyy"];
    
    return @{@"name":self.name,@"date":[df stringFromDate:self.dateJoined]};
}

- (NSArray *)tr_sectiontablerepresentation {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    return @[ @{@"titles": @[@"Name", @"Age"], @"values":@[self.name, [nf stringFromNumber:self.age]]},
              @{@"titles": @[@"E-mail", @"Phone", @"Address"], @"values":@[self.email,self.phone,self.address]},
              @{@"titles": @[@"Purchased", @"Account Number"], @"values":@[[nf stringFromNumber:self.purchased],[nf stringFromNumber:self.acct]]}];
}
@end
