//
//  CVCustomerViewController.h
//  CustomerView
//
//  Created by Peter Malmgren on 12/19/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVCustomerViewController : UIViewController
- (void) ActionMethod:(id)sender;
@end
