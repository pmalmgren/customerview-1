//
//  CVCustomerDetailViewViewController.h
//  CustomerView
//
//  Created by Peter Malmgren on 12/19/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CVCustomer+TableRepresentation.h"

@interface CVCustomerDetailViewController : UITableViewController

- (id) initWithStyle:(UITableViewStyle)style index:(long int)index;
- (void) ActionMethod:(id)sender;
- (void) updateNotes;

@end
