//
//  CVCustomer+TableRepresentation.h
//  CustomerView
//
//  Created by Peter Malmgren on 12/19/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import "CVCustomer.h"

@interface CVCustomer (TableRepresentation)
- (NSArray *)tr_sectiontablerepresentation;
- (NSDictionary *)tr_cellrepresentation;
@end
