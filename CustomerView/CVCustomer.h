//
//  CVCustomer.h
//  CustomerView
//
//  Created by Peter Malmgren on 12/19/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CVCustomer : NSObject

@property (nonatomic, copy) NSString *name, *pictureUrl, *email, *address, *phone, *customerNotes;
@property (nonatomic, copy) NSNumber *purchased, *acct, *age;
@property (nonatomic, copy) NSDate *dateJoined;
@property (nonatomic, copy) NSMutableArray *purchaseHistory;

//- (id) initWithName:(NSString *)name pictureUrl:(NSString *)pictureUrl age:(NSString *)age acct:(NSString *)acct purchaseHistoryUrl:(NSString *)purchaseHistoryUrl dateJoined:(NSString *)dateJoined email:(NSString *)email address:(NSString *)address purchased:(NSString *)purchased phone:(NSString *)phone;
- (id) initWithYear:(int)year month:(int)month day:(int)day hour:(int)hour name:(NSString *)name pictureUrl:(NSString *)pictureUrl age:(NSNumber *)age acct:(NSNumber *)acct email:(NSString *)email address:(NSString *)address purchased:(NSNumber *)purchased phone:(NSString *)phone;
- (NSString *) getImageUrl;
- (NSString *) getNotes;
- (void) setNotes:(NSString *)newNote;

@end
