//
//  LibraryAPI.m
//  CustomerView
//
//  Created by Peter Malmgren on 12/19/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import "LibraryAPI.h"
#import "PersistencyManager.h"

@interface LibraryAPI() {
    PersistencyManager *persistencyManager;
    BOOL isOnline;
}
@end

@implementation LibraryAPI

- (id) init {
    self = [super init];
    if (self) {
        persistencyManager = [[PersistencyManager alloc] init];
        isOnline = NO;
    }
    return self;
}

- (void) setCustomerNoteAtIndex:(long int)index note:(NSString *)note {
    [persistencyManager setCustomerNoteAtIndex:index note:note];
}

- (NSArray *) getCustomers {
    return [persistencyManager getCustomers];
}

- (CVCustomer *) getCustomerAtIndex:(long int)index {
    return [persistencyManager getCustomerAtIndex:index];
}

- (void) addCustomer:(CVCustomer *)customer atIndex:(long int)index {
    [persistencyManager addCustomer:customer atIndex:index];

    if (isOnline) {
        // update the database
    }
}

- (void) deleteCustomerAtIndex:(long int)index {
    [persistencyManager deleteCustomerAtIndex:index];

    if (isOnline) {
        // update the database
    }
}

- (void) reorganizeByKey:(NSString *)key {
    [persistencyManager reorganizeByKey:key];
}

+ (LibraryAPI*)sharedInstance
{
    static LibraryAPI *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[LibraryAPI alloc] init];
    });
    return _sharedInstance;
}
@end
