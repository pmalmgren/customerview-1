//
//  CVCustomerViewController.m
//  CustomerView
//
//  Created by Peter Malmgren on 12/19/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import "CVCustomerViewController.h"
#import "LibraryAPI.h"
#import "CVCustomer+TableRepresentation.h"
#import "CVCustomerDetailViewController.h"

@interface CVCustomerViewController () <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate> {
    UITableView *dataTable;
    NSArray *allCustomers;
}
@end

@implementation CVCustomerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Customers";
    allCustomers = [[LibraryAPI sharedInstance] getCustomers];
    dataTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStyleGrouped];
    dataTable.delegate = self;
    dataTable.dataSource = self;
    dataTable.backgroundView = nil;
    dataTable.rowHeight = 60;
    [self.view addSubview:dataTable];
    
    // sort
    UIBarButtonItem *sortButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(sortSelector:)];
    sortButton.title = @"Sort";
    [self.navigationItem setLeftBarButtonItem:sortButton];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [allCustomers count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    NSDictionary *currentCustomerData = [allCustomers[indexPath.row] tr_cellrepresentation];
    cell.textLabel.text = [currentCustomerData valueForKey:@"name"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Customer Since: %@",[currentCustomerData valueForKey:@"date"]];
    
    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
    [myButton setTag:indexPath.row];
    myButton.frame = CGRectMake(10.0, 0.0, 20, 20);
    cell.accessoryView = myButton;
    [cell.contentView addSubview:myButton];
    cell.backgroundColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1];
    [myButton addTarget:self action:@selector(ActionMethod:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewHeaderFooterView *sectionHeaderView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"header"];
    
    if (!sectionHeaderView) {
        sectionHeaderView = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:@"header"];
        
        switch (section)
        {
            case 1:
                sectionHeaderView.textLabel.text = @"Customer List";
                break;
            default:
                sectionHeaderView.textLabel.text = @"Customer Information";
                break;
        }
    }
    
    return sectionHeaderView;
}

#pragma mark Action methods and selectors

- (void) ActionMethod:(UIButton*)sender {
    long int index = sender.tag;
    CVCustomerDetailViewController *Obj = [[CVCustomerDetailViewController alloc] initWithStyle:UITableViewStyleGrouped index:index];
    [self.navigationController pushViewController:Obj animated:YES];
}


- (void) sortSelector:(id)sender {
    UIActionSheet *sortActionSheet = [[UIActionSheet alloc] initWithTitle:@"Sort customers by" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Time with company",@"Amount purchased",nil];
    [sortActionSheet showInView:self.view];
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [[LibraryAPI sharedInstance] reorganizeByKey:@"dateJoined"];
            allCustomers = [[LibraryAPI sharedInstance] getCustomers];
            [dataTable reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        case 1:
            [[LibraryAPI sharedInstance] reorganizeByKey:@"purchased"];
            allCustomers = [[LibraryAPI sharedInstance] getCustomers];
            [dataTable reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        default:
            break;
    }
    for (CVCustomer *c in allCustomers) {
        NSLog(@"%@",c.name);
    }
}

@end
