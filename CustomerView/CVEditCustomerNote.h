//
//  CVEditCustomerNote.h
//  CustomerView
//
//  Created by Peter Malmgren on 12/22/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVEditCustomerNote : UIViewController

- (id)initWithCustomerNote:(NSString *)customerNote index:(long int) index;
- (void)cancelSelector:(id)sender;
- (void)saveSelector:(id)sender;

@end
