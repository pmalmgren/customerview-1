//
//  CVCustomerDetailViewViewController.m
//  CustomerView
//
//  Created by Peter Malmgren on 12/19/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import "CVCustomerDetailViewController.h"
#import "CVCustomer+TableRepresentation.h"
#import "LibraryAPI.h"
#import "CVEditCustomerNote.h"

@interface CVCustomerDetailViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation CVCustomerDetailViewController {
    CVCustomer *currentCustomer;
    NSArray *currentCustomerData;
    NSURL *imageUrl;
    long int currentIndex;
    UITableView *currentCustomerDataTable;
}

#pragma mark - Initialization functions

- (id) initWithStyle:(UITableViewStyle)style index:(long int)index
{
    self = [super initWithStyle:style];
    if (self) {
        currentCustomer = [[LibraryAPI sharedInstance] getCustomerAtIndex:index];
        currentCustomerData = [currentCustomer tr_sectiontablerepresentation];
        imageUrl = [NSURL URLWithString:[currentCustomer getImageUrl]];
        currentIndex = index;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TextCompletedNotification" object:self];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Details";
    currentCustomerDataTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStyleGrouped];
    currentCustomerDataTable.delegate = self;
    currentCustomerDataTable.dataSource = self;
    currentCustomerDataTable.backgroundView = nil;
    [self.view addSubview:currentCustomerDataTable];
    self.automaticallyAdjustsScrollViewInsets = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotes) name:@"NoteUpdate" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [currentCustomerData count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    else if (section > 0 && section < [currentCustomerData count]) {
        return [currentCustomerData[section][@"titles"] count];
    }
    else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) return 80.0f;
    else return 40.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"cell"];
    UITableViewCell *cell = [currentCustomerDataTable dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.section == 0) {
        // customer image
        NSData *data = [[NSData alloc] initWithContentsOfURL:imageUrl];
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,50,50)];
        imgView.image = [[UIImage alloc] initWithData:data];
        cell.imageView.image = imgView.image;
        // customer name
        cell.textLabel.text = currentCustomerData[indexPath.section][@"values"][indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:25];
    }
    
    if (indexPath.section > 0 && indexPath.section < [currentCustomerData count]) {
        // Account and contact information
        cell.textLabel.text = currentCustomerData[indexPath.section][@"titles"][indexPath.row];
        cell.detailTextLabel.text = currentCustomerData[indexPath.section][@"values"][indexPath.row];
    }
    
    else if (indexPath.section == [currentCustomerData count]) {
        cell.textLabel.text = [currentCustomer getNotes];
        UIButton *myButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [myButton setTitle:@"Edit >" forState:UIControlStateNormal];
        myButton.frame = CGRectMake(10.0, 0.0, 50, 20);
        cell.accessoryView = myButton;
        [cell.contentView addSubview:myButton];
        cell.backgroundColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1];
        [myButton addTarget:self action:@selector(ActionMethod:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.backgroundView = [[UIView alloc] initWithFrame:cell.bounds];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionIdentifier = [NSString stringWithFormat:@"section%li",section];
    
    UITableViewHeaderFooterView *sectionHeaderView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:sectionIdentifier];
    
    if (!sectionHeaderView) {
        sectionHeaderView = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:sectionIdentifier];
    }
    
    sectionHeaderView.frame = CGRectMake(0,0,200,40);
    
    switch (section) {
        case 0:
            sectionHeaderView.textLabel.text = @"";
            break;
        case 1:
            sectionHeaderView.textLabel.text = @"Contact Information";
            break;
        case 2:
            sectionHeaderView.textLabel.text = @"Purchasing Account Information";
            break;
        case 3:
            sectionHeaderView.textLabel.text = @"Customer Notes";
            break;
        default:
            sectionHeaderView.textLabel.text = @"Customer Information";
            break;
    }
    
    return sectionHeaderView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    NSString *sectionIdentifier = [NSString stringWithFormat:@"footersection%li",section];
    
    UITableViewHeaderFooterView *sectionFooterView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:sectionIdentifier];
    
    if (!sectionFooterView) {
        sectionFooterView = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:sectionIdentifier];
    }
    
    sectionFooterView.frame = CGRectMake(0,0,0,0);
    sectionFooterView.textLabel.text = @"";
    return sectionFooterView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}


#pragma mark - Action controllers

- (void) ActionMethod:(UIButton *)sender {
    CVEditCustomerNote *Obj = [[CVEditCustomerNote alloc] initWithCustomerNote:[currentCustomer getNotes] index:currentIndex];
    [self.navigationController pushViewController:Obj animated:YES];
}

- (void) updateNotes {
    currentCustomer = [[LibraryAPI sharedInstance] getCustomerAtIndex:currentIndex];
    NSLog(@"%@",[currentCustomer getNotes]);
    currentCustomerData = [currentCustomer tr_sectiontablerepresentation];
    [currentCustomerDataTable reloadSections:[[NSIndexSet alloc] initWithIndex:3] withRowAnimation:UITableViewRowAnimationAutomatic];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
