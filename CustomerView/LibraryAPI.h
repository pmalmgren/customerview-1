//
//  LibraryAPI.h
//  CustomerView
//
//  Created by Peter Malmgren on 12/19/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CVCustomer.h"

@interface LibraryAPI : NSObject

+ (LibraryAPI*)sharedInstance;
- (NSArray *) getCustomers;
- (void) addCustomer:(CVCustomer *)customer atIndex:(long int)index;
- (void) deleteCustomerAtIndex:(long int)index;
- (void) setCustomerNoteAtIndex:(long int)index note:(NSString *)note;
- (CVCustomer *) getCustomerAtIndex:(long int)index;
- (void) reorganizeByKey:(NSString *)key;

@end
