//
//  PersistencyManager.m
//  CustomerView
//
//  Created by Peter Malmgren on 12/19/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import "PersistencyManager.h"

@implementation PersistencyManager {
    NSMutableArray *customers;
}

- (id) init {
    self = [super init];
    if (self) {
        customers = [NSMutableArray arrayWithArray:
                     @[[[CVCustomer alloc] initWithYear:2012 month:12 day:23 hour:12 name:@"Worf" pictureUrl:@"http://upload.wikimedia.org/wikipedia/en/e/ed/WorfTNG.jpg" age:@30 acct:@800241252 email:@"worf@enterprise.mil" address:@"2342 Deck C" purchased:@300 phone:@"800-212-4214"],
                       [[CVCustomer alloc] initWithYear:2010 month:8 day:10 hour:9 name:@"Data" pictureUrl:@"http://upload.wikimedia.org/wikipedia/en/thumb/0/09/DataTNG.jpg/250px-DataTNG.jpg" age:@23 acct:@800241243 email:@"data@enterprise.mil" address:@"1920 Deck D" purchased:@9000 phone:@"800-212-4215"],
                       [[CVCustomer alloc] initWithYear:2008 month:2 day:12 hour:5 name:@"Captain Picard" pictureUrl:@"http://upload.wikimedia.org/wikipedia/en/2/20/Captain_Picard_Chair.jpg" age:@50 acct:@800241244 email:@"captain@enterprise.mil" address:@"USS Enterprise" purchased:@40000 phone:@"800-212-4214"],
                       [[CVCustomer alloc] initWithYear:2013 month:6 day:2 hour:2 name:@"Libby the Cat" pictureUrl:@"https://scontent-a-atl.xx.fbcdn.net/hphotos-prn2/1451981_10100922714090611_714527485_n.jpg" age:@5 acct:@800241245 email:@"annoyingcat@cats.com" address:@"154 Franklin Ave" purchased:@-500 phone:@"704-236-0267"],
                       [[CVCustomer alloc] initWithYear:2006 month:9 day:19 hour:12 name:@"Galadriel" pictureUrl:@"http://upload.wikimedia.org/wikipedia/commons/thumb/2/28/Galadriel_Hildebrandt_Calendar_1978.jpg/240px-Galadriel_Hildebrandt_Calendar_1978.jpg" age:@9000 acct:@800241246 email:@"galadriel@lothlorien.net" address:@"1900 Lothlorien Lane" purchased:@9923.3 phone:@"800-969-5421"]]];
    }
    return self;
}

- (void) reorganizeByKey:(NSString *)key {
    if ([key isEqual:@"purchased"]) {
        customers = [NSMutableArray arrayWithArray:[customers sortedArrayUsingComparator: ^(CVCustomer *c1, CVCustomer *c2) {
            return [c2.purchased compare:c1.purchased];
        }]];
    }
    else if ([key isEqual:@"dateJoined"]) {
        customers = [NSMutableArray arrayWithArray:[customers sortedArrayUsingComparator: ^(CVCustomer *c1, CVCustomer *c2) {
            return [c2.dateJoined compare:c1.dateJoined];
        }]];
    }
    else {
        NSLog(@"Error");
    }
    
}

- (NSArray *) getCustomers {
    return customers;
}

- (CVCustomer *) getCustomerAtIndex:(long int)index {
    return [customers objectAtIndex:index];
}

- (void) addCustomer:(CVCustomer *)customer atIndex:(long int)index {
    if (customers.count >= index)
        [customers insertObject:customer atIndex:index];
    else
        [customers removeObjectAtIndex:index];
}

- (void) deleteCustomerAtIndex:(long int)index {
    [customers removeObjectAtIndex:index];
}

- (void) setCustomerNoteAtIndex:(long int)index note:(NSString *)newNote {
    [[customers objectAtIndex:index] setCustomerNotes:newNote];
}

- (NSString *) getCustomerNoteAtIndex:(long int)index {
    return [[customers objectAtIndex:index] getNotes];
}

@end
