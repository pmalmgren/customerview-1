//
//  CVAppDelegate.h
//  CustomerView
//
//  Created by Peter Malmgren on 12/19/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CVCustomerViewController.h"

@interface CVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CVCustomerViewController *viewController;
@property (strong, nonatomic) UINavigationController *tableViewNavigationController;

@end
