//
//  PersistencyManager.h
//  CustomerView
//
//  Created by Peter Malmgren on 12/19/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CVCustomer.h"

@interface PersistencyManager : NSObject

- (NSArray *) getCustomers;
- (NSString *) getCustomerNoteAtIndex:(long int)index;
- (CVCustomer *) getCustomerAtIndex:(long int)index;
- (void) addCustomer:(CVCustomer *)customer atIndex:(long int)index;
- (void) deleteCustomerAtIndex:(long int)index;
- (void) setCustomerNoteAtIndex:(long int)index note:(NSString *)newNote;
- (void) reorganizeByKey:(NSString *)key;

@end
