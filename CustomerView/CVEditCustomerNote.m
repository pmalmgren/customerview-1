//
//  CVEditCustomerNote.m
//  CustomerView
//
//  Created by Peter Malmgren on 12/22/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import "CVEditCustomerNote.h"
#import "LIbraryAPI.h"

@interface CVEditCustomerNote () <UITextViewDelegate>

@end

@implementation CVEditCustomerNote {
    NSString *customerNote;
    UITextView *editCustomerNoteField;
    long int customerIndex;
}

- (id)initWithCustomerNote:(NSString *)currentNote index:(long int)index {
    self = [super init];
    
    if (self) {
        customerNote = currentNote;
        customerIndex = index;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.title = @"Edit Notes";
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Add the navigation bar buttons
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelSelector:)];
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveSelector:)];
    cancelButton.title = @"Cancel";
    saveButton.title = @"Save";
    [self.navigationItem setLeftBarButtonItem:cancelButton];
    [self.navigationItem setRightBarButtonItem:saveButton];
    
    // Add the text field
    editCustomerNoteField = [[UITextView alloc] initWithFrame:CGRectMake(0,0,300,300)];
    editCustomerNoteField.delegate = self;
    editCustomerNoteField.text = customerNote;
    
    [self.view addSubview:editCustomerNoteField];
}

- (void) cancelSelector:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) saveSelector:(id)sender {
    [[LibraryAPI sharedInstance] setCustomerNoteAtIndex:customerIndex note:editCustomerNoteField.text];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NoteUpdate" object:self];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
