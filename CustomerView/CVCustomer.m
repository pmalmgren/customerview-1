//
//  CVCustomer.m
//  CustomerView
//
//  Created by Peter Malmgren on 12/19/13.
//  Copyright (c) 2013 Peter Malmgren. All rights reserved.
//

#import "CVCustomer.h"

@implementation CVCustomer

//- (id) initWithName:(NSString *)name pictureUrl:(NSString *)pictureUrl age:(NSString *)age acct:(NSString *)acct purchaseHistoryUrl:(NSString *)purchaseHistoryUrl dateJoined:(NSString *)dateJoined email:(NSString *)email address:(NSString *)address purchased:(NSString *)purchased phone:(NSString *)phone {
//    self = [super init];
//    if (self) {
//        _name = name;
//        _pictureUrl = pictureUrl;
//        _age = age;
//        _acct = acct;
//        _purchaseHistory = nil;
//        _customerNotes = @"No notes";
//        _dateJoined = dateJoined;
//        _email = email;
//        _phone = phone;
//        _address = address;
//        _purchased = purchased;
//    }
//    return self;
//}

- (id) initWithYear:(int)year month:(int)month day:(int)day hour:(int)hour name:(NSString *)name pictureUrl:(NSString *)pictureUrl age:(NSNumber *)age acct:(NSNumber *)acct email:(NSString *)email address:(NSString *)address purchased:(NSNumber *)purchased phone:(NSString *)phone {
    
    self = [super init];
    
    if (self) {
        _dateJoined = [[NSDate alloc] initWithTimeIntervalSince1970:NSTimeIntervalSince1970];
        NSDateComponents *customerDateComponents = [NSDateComponents new];
        NSCalendar *greg = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        customerDateComponents.year = year;
        customerDateComponents.month = month;
        customerDateComponents.day = day;
        customerDateComponents.hour = hour;
        _dateJoined = [greg dateFromComponents:customerDateComponents];
        
        _name = name;
        _pictureUrl = pictureUrl;
        _age = age;
        _acct = acct;
        _purchaseHistory = nil;
        _customerNotes = @"No Notes";
        _email = email;
        _phone = phone;
        _address = address;
        _purchased = purchased;
    }
    
    return self;
}

- (NSString *) getImageUrl {
    return _pictureUrl;
}

- (NSString *) getNotes {
    return _customerNotes;
}

- (void) setNotes:(NSString *)customerNotes {
    _customerNotes = customerNotes;
}

@end
